import Express, { Request } from "express";
import { createConnection } from "typeorm";
import * as dotenv from "dotenv";
import cors from "cors";

import GenericRoutes from "./app/routes/generic.routes"
import GenericController from "./app/controllers/generic.controller";
import HTTPError from './app/middleware/middleware.errors';

class App {
    // Application is the express application for routing, registering middleware, etc.
    private application: Express.Application = Express();

    // Routes are all of the routes. They should all be of @type {Router}.
    private router: Express.Router = Express.Router();

    // The main function for starting the server.
    public startServer(): void {
        // Configure environment variables. See README for instructions.
        dotenv.config();
        this.registerDatabase();
        this.application.use(cors());
        this.registerRoutes();
        this.registerMiddleware();
        this.registerExpressApplication();
    }

    /**
     * Regsiters all Express middleware.
     */
    private registerMiddleware(): void {
        this.application.use((err: Error, _req: Request, res: any, _next: any) => {
            var status = 500;
            res.status(status).send(new HTTPError(err.message, status, err))
        })
    }

    /**
     * Registers all routers and routes.
     */
    private registerRoutes(): void {
        console.log("Registering all routes.");

        // First, you create a new Router object using whatever controller you have built.
        const genericRoutes = new GenericRoutes(new GenericController, "/yourendpoint");

        // Next, you send the router in to the registerRoutes. 
        // If you use the router.ts class correctly (inherit it in every router) then this should just be step by step the same process.
        genericRoutes.registerRoutes(this.router);


        this.application.use("/api/v1", this.router);
    }

    /**
     * Registers the database connection. Now everything using TypeORM can access the database.
     * Why TypeORM?
     * It allows for anyone to make queries. You don't have to know SQL, just how to write javascript.
     * 
     * For instance (MySQL):
     * SELECT * FROM car ORDER BY creation DESC LIMIT 1
     * 
     * versus (TypeORM):
     * await dbCon.getRepository(car).find().order("creation", "desc").getOne();
     * 
     * @returns {void} FAIAP Doesn't return anything.
     */
    private async registerDatabase(): Promise<void> {
        try {
            await createConnection({
                type: "mysql",
                host: process.env.MYSQL_CONNECTION_URL,
                port: 3306,
                username: process.env.MYSQL_USERNAME,
                password: process.env.MYSQL_PASSWORD,
                database: process.env.MYSQL_DATABASE,
                entities: [__dirname + "/app/entities/*.entity.{js,ts}"]
            });
        } catch (e) {
            // just log the error for now.
            console.log(e);
        }

    }

    /**
     * Starts the Express application on a port.
     */
    private registerExpressApplication(): void {
        this.application.listen(process.env.HOST_PORT, 
                                () => console.log(`The development server is currently running on port ${process.env.HOST_PORT}`))
    }
}

// This function is so that the C++ programmers don't get upset at me - STB
function main() {
    // Create a new instance of the server.
    const app = new App();
    // Start the server.
    app.startServer();
}

// Start the program.
main();