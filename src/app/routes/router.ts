import Express from "express";
import Controller from "../controllers/controller";

class Router<T extends Controller> {
    protected controller: T;
    private routePath: string;

    public registerRoutes(router: Express.Router) {
        router.route(this.routePath)
            .post(this.controller.create)
            .get(this.controller.read)
            .put(this.controller.update)
            .delete(this.controller.delete);
    }

    /**
     * @param c Defines the controller that the path should use. Since every controller has `create`, `read`, `update`, and `delete`, this will never fail.
     * @param routePath Defines the path which the general router should be registered on.
     * I.E. /car
     */
    constructor(c: T, routePath: string) {
        this.controller = c;
        this.routePath = routePath;
    }
}

export default Router