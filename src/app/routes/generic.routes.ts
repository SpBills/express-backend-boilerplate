import Router from './router';
import GenericController from '../controllers/generic.controller';

/**
 * For now, nothing needs to be defined in here.
 * Whenever the scope of this goes out of the original Router,
 * for instance getting individual entities,
 * that is whenever you need to start adding functions to this.
 */
class GenericRoutes extends Router<GenericController> {}

export default GenericRoutes