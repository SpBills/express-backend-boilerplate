import Controller from './controller';
import { getConnection } from 'typeorm';
import { Request, Response } from 'express';
import GenericEntity from "../entities/generic.entity"

/**
 * A controller that defines all of the behavior whenever accessing properties on a Car.
 */
class CarController implements Controller {
    create(): Promise<void> {
        throw new Error("Method not implemented.");
    }
    async read(_req: Request, res: Response): Promise<void> {
        const data = await getConnection().getRepository(GenericEntity).find();

        res.json(data);
    }
    update(): Promise<void> {
        throw new Error("Method not implemented.");
    }
    delete(): Promise<void> {
        throw new Error("Method not implemented.");
    }
}

export default CarController